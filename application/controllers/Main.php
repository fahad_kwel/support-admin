<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

        public function index(){
            $this->load->view('contact');
            
        }
        public function contact(){
            $this->load->view('contact');
        }

        public function reminder(){
            $this->load->view('reminder');
        }

        public function billing(){
            $this->load->view('billing');
        }

        public function client(){
            $this->load->model('Client_Model');
            $test['records'] = $this->Client_Model->getData();

             $this->load->view('clientTest', $test);
        }
}