<?php include_once('layout/header.php'); ?>
<div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
  <div class="col-md-4">   
<ul class="custom-breadcrumb">
  <li><a href="#">Home &nbsp;/&nbsp;</a></li>
  <li><a href="#">Library&nbsp;/&nbsp;</a></li>
  <li class="active">Data</li>
</ul>
</div>

    
<div class="col-md-8">
  <div class="col-md-3">     
    <div class="form-group">
            
        <input class="form-control input-sm" type="text" id="inputSmall" placeholder="Search Keyword">
        
    </div>
  </div>
  <div class="col-md-3">  
    <div class="form-group">
          
        <input class="form-control input-sm" type="text" id="inputSmall" placeholder="Search Keyword">
    </div>
  </div>
  <div class="col-md-3">  
    
  <div class="form-group">
      
      
        <select class="form-control input-sm" id="select">
          <option>Newly Registered</option>
          <option>Trail Started</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
        
      
    </div>

  </div>  

</div>
    </div>
    </div>
<div class="container-fluid">
<div class="table-responsive">
<table class="table table-striped table-hover table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Email</th>
      <th>Second Email</th>
      <th>Skype ID</th>
      <th>Phone</th>
      <th>Country</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
      <td>Column content</td>
    </tr>
   </tbody>
</table>
</div>
</div>
<?php include_once ('layout/footer.php');?>